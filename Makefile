NAME     = _letter
TEX      = $(NAME).tex
FULLNAME = Lettre

DIRECTORIES = $(filter-out "." ".git",$(shell find . -maxdepth 1 -type d -printf "\"%f\" "))
TEMPLATE    = Template

TEXMK = latexmk -lualatex -interaction=nonstopmode
QPDF  = qpdf --linearize --replace-input
RM    = rm -f
CP    = cp -r

all: build

new:
	@echo Nom du dossier ?
	@read line; echo $$line | xargs -i $(CP) '$(TEMPLATE)' {}/

build:
	@for dir in $(filter-out "$(TEMPLATE)",$(DIRECTORIES)); do \
		cd "$$dir"; \
		$(TEXMK) "$(TEX)"; \
		$(QPDF) "$(NAME).pdf" 2>/dev/null |:; \
		mv $(NAME).pdf "../$(FULLNAME)-$$dir.pdf"; \
		cd ..; \
	done;

EXTS = aux log tns fdb_latexmk fls synctex.gz pdf out
clean:
	@for dir in $(DIRECTORIES); do \
		cd "$$dir"; \
		$(RM) $(foreach e,$(EXTS),$(NAME).$(e)); \
		cd ..; \
	done;

clean-full:
	$(MAKE) clean
	$(RM) *.pdf
