\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{../easyLetter}[
  2023/04/04 Small class based on 'lettre' to quickly write a letter (FR)]

\LoadClass[12pt,a4paper]{lettre}

% Langue
\RequirePackage[french]{babel}
\RequirePackage{csquotes}
\MakeOuterQuote{"}

% Police
\RequirePackage{microtype}
\RequirePackage{fontspec}
\setmainfont[Numbers={Monospaced}]{Fira Sans}
\setmonofont{Fira Code}

% Retire la ligne ajoutée pour la séparation entête/corps de la lettre
\renewcommand*{\rule@length}{0}

% Titre
\newcommand*{\titre}[1]{\def\@title{#1}}

% Auteur
\newcommand*{\auteur}[1]{\def\@author{#1}}

% Sujet
\def\@subject{\,}
\newcommand*{\sujet}[1]{\def\@subject{#1}}

% Mot-clés
\def\@keywords{\,}
\newcommand*{\motscles}[1]{\def\@keywords{\if#1\empty\else, #1\fi}}

% Clickable links + metadata
\AtEndPreamble{
  \RequirePackage[
    pdfauthor={\@author},
    pdftitle={\@title},
    pdfsubject={\@subject},
    pdfkeywords={\@author, lettre\@keywords},
    pdfcreator={LaTeX with 'easyLetter' preset},
    % Hide links
    hidelinks
  ]{hyperref}
}

% Introduction
\newcommand{\ouverture}[1]{\def\@opening{#1}}

% Conclusion
\newcommand{\cloture}[1]{\def\@closing{#1}}

% Destinataire
\newcommand{\destinataire}[3]{\def\@receivername{#1}\def\@receiverAddress{#2}\def\@receiverCity{#3}}

% Contenu de la lettre
\newenvironment{easyLetter}{
  \begin{letter}{
      % Destinataire
      \@receivername \\
      \@receiverAddress \\
      \textsc{\@receiverCity}
    }
    \conc{\@title}
    \opening{\@opening}}{
    \closing{\@closing}
  \end{letter}
}
