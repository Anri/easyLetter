# EasyLetter

Dépend de Fira Sans et Fira Code.

## Customisation

- Modifier vos coordonnées dans [lettre-expediteur.cfg](./lettre-expediteur.cfg)
- Modifier [le template](./Template/_letter.tex)
  - Titre du document (= Objet de la lettre)
  - Coordonnées du destinataire
  - Votre identité pour les métadonnées du PDF (facultatif)

## Utilisation

- Créer une nouvelle lettre :

```sh
make new
```

- Génère les PDFs

```sh
make
```

- Supprime les fichiers temporaires

```sh
make clean
```

- Supprime les fichiers temporaires + les PDFs générés

```sh
make clean-full
```

## Exemple

![](https://i.imgur.com/psYkm1A.jpg)
